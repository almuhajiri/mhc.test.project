# README

MHC Test Project Mobile Application based on React Native Framework. Built with React Native 0.67.2 latest version.

## ScreenShot App

<img src="screenshotHome.JPEG" alt="home" width="60%"/>
<img src="screenshotDetail.JPEG" alt="detail" width="60%"/>

## System Requirements

- NodeJS version 12 or later
- NPM version 6 or later
- Python2
- JDK 8
- CocoaPods
- Android SDK / Xcode development setup [[Read More Here](https://github.com/facebook/react-native-website/blob/master/docs/getting-started.md)]

## Application Requirements

- Android OS SDK 21 (Lollipop)
- iOS 10
- Minimum Free Space 200MB
- Minimum Memory 2GB

## Installation

First of all, install npm package dependencies

```
npm install
```

or if using yarn:

```
yarn install
```

# Run Application [Android]

Below command is used for running metro packager and build and install android debug APK file to device or emulator

```
npx react-native run-android
```

Or if you want run a stand alone metro packager only, use below command:

```
yarn start
```

And If you want to build APK file only use below command:

- Build debug version

```
// on Windows
> cd android
> .\gradlew assembleDebug

// on Mac OS
$ cd android
$ chmod +x gradlew
$ ./gradlew assembleRelease
```

- Build release version

```
// on Windows
> cd android
> .\gradlew assembleRelease

// on Mac OS
$ cd android
$ chmod +x gradlew
$ ./gradlew assembleRelease
```

## Run Application [iOS]

Below command is used for running metro packager and build and install iOS debug IPA file to device or emulator

```
npx react-native run-ios
```

## Application Structure

```
- android/                                      -> native code android
- ios/                                          -> native code iOS
- src/
  - app/                                        -> Relate to whole application
    - config
      - index.js                                -> base Url and Endpoint
    - localDb/
      - models/                                 -> WatermelonDb model
      - index.js                                -> WatermelonDb setup file
      - tables.js                               -> localdb table list
      - migrations.js                           -> localdb migrations list
    - Helper.js                                 -> General method helper & constant
    - index.js                                  -> Navigator Route here
    - Redux.js                                  -> Redux configuration
    - Routes.js                                 -> Navigation Route Helper
    - Themes.js                                 -> Global Stylesheet
  - compmonents/                                -> Relate to whole basic component
    - [component group]/
      - index.js
  - modules/                                    -> Relate to whole apps module
    - [module name]/
      - Component/                              -> PURE component should be here
        - [module component].js
      - Container/                              -> component logic code should be here
        - [module component container].js
      - Stores/                                 -> module redux store
        - Actions/                              -> module redux action
          - [module action].js
        - Reducers/                             -> module redux reducer
          - [module reducer].js
```
