import React, { Component } from 'react';
import { Text, View, Animated, Dimensions, StyleSheet } from 'react-native';
import { PinchGestureHandler, State } from 'react-native-gesture-handler';

const screen = Dimensions.get('window');

const USE_NATIVE_DRIVER = true;

const styles = StyleSheet.create({
  image: {
    width: screen.width,
    height: screen.width,
  },
});

export class index extends Component {
  _baseScale = new Animated.Value(1);
  _pinchScale = new Animated.Value(1);
  _scale = Animated.multiply(this._baseScale, this._pinchScale);
  _lastScale = 1;

  _onPinchGestureEvent = Animated.event(
    [{ nativeEvent: { scale: this._pinchScale } }],
    { useNativeDriver: USE_NATIVE_DRIVER },
  );

  _onPinchHandlerStateChange = (event) => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      this._lastScale *= event.nativeEvent.scale;
      this._baseScale.setValue(this._lastScale);
      this._pinchScale.setValue(1);
    }
  };
  render() {
    const { imageUrl, styles } = this.props;

    return (
      <PinchGestureHandler
        onGestureEvent={() => this._onPinchGestureEvent}
        onHandlerStateChange={this._onPinchHandlerStateChange}
      >
        <View collapsable={false}>
          <Animated.Image
            source={{
              uri: imageUrl,
            }}
            style={[
              styles,
              {
                transform: [{ perspective: 200 }, { scale: this._scale }],
              },
            ]}
          />
        </View>
      </PinchGestureHandler>
    );
  }
}

export default index;
