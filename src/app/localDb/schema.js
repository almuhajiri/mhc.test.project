import { appSchema, tableSchema } from '@nozbe/watermelondb';

import Tables from './tables';

export default appSchema({
  version: 1,
  tables: [tableSchema(Tables.contents)],
});
