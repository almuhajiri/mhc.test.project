import { Model } from '@nozbe/watermelondb';
import { field, text } from '@nozbe/watermelondb/decorators';

export default class Content extends Model {
  static table = 'contents';

  @text('contentId') contentId;

  @text('url') url;

  @field('category') category;

  @text('category_name') category_name;

  @field('isBookmark') isBookmark;
}
