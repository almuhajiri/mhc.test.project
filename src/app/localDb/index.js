import { Platform } from 'react-native';
import { Database } from '@nozbe/watermelondb';
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite';

import models from './models';
import schema from './schema';
import migrations from './migrations';

const adapter = new SQLiteAdapter({
  schema,
  migrations,
  dbName: 'MHCTestProjectDb',
  jsi: true /* Platform.OS === 'ios' */,
  // (optional, but you should implement this method)
  onSetUpError: (error) => {
    console.log('setUp Database error! ', error);
  },
});

export const database = new Database({
  adapter,
  modelClasses: models,
});

export const contentsCollection = database.get('contents');
