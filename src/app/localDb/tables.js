const Tables = {
  contents: {
    name: 'contents',
    columns: [
      { name: 'contentId', type: 'string' },
      { name: 'url', type: 'string' },
      { name: 'category', type: 'number' },
      { name: 'category_name', type: 'string' },
      { name: 'isBookmark', type: 'boolean' },
    ],
  },
};

export default Tables;
