import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import store from './redux';
import Routes from './Routes';

import HomeContainer from '../modules/Home/Containers/HomeContainer';
import DetailContentContainer from '../modules/DetailContent/Containers/DetailContentContainer';

const Stack = createNativeStackNavigator();

const ApplicationStack = (props) => {
  const { route } = props;

  const initialRouteName = Routes.Login;

  const initialParams = {
    // ...route.params,
  };

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={initialRouteName}>
        <Stack.Screen
          name={Routes.Home}
          component={HomeContainer}
          initialParams={initialParams}
        />
        <Stack.Screen
          name={Routes.DetailContent}
          component={DetailContentContainer}
          initialParams={initialParams}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const app = () => {
  return (
    <Provider store={store}>
      <ApplicationStack />
    </Provider>
  );
};

export default app;
