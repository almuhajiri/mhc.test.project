import { combineReducers, createStore, applyMiddleware } from 'redux';

import Route from './Routes';
import HomeReducer from '../modules/Home/Stores/HomeReducer';
import DetailContentReducer from '../modules/DetailContent/Stores/DetailContentReducer';

const reducers = combineReducers({
  HomeReducer,
  DetailContentReducer,
});

const store = createStore(reducers);

export default store;
