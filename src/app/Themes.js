const COLORS = {
  black: '#333333',
  grey1: '#4E5258',
  grey2: '#AAAAAA',
  grey3: '#f0f0f0',
  white: '#ffffff',
  blue: '#2F80ED',
  tosca: '#31D0AA',
  red: '#E74C3C',
};

const FONTS = {};

const Themes = {
  COLORS,
  FONTS,
};

export default Themes;
