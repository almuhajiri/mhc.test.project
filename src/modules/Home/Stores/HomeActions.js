import React from 'react';
import axios from 'axios';
import { Q } from '@nozbe/watermelondb';
import Route from '../../../app/Routes';
import { ToastAndroid } from 'react-native';

import { getListContent } from '../../../app/config';
import { contentsCollection, database } from '../../../app/localDb';

export const SET_FILTER = 'SET_FILTER';
export const SET_KEYWORD = 'SET_KEYWORD';
export const RESET_FILTER = 'RESET_FILTER';
export const RESET_LIST_CONTENT = 'RESET_LIST_CONTENT';
export const SET_DATA_LIST_CONTENT = 'SET_DATA_LIST_CONTENT';
export const GET_DATA_LIST_CONTENT = 'GET_DATA_LIST_CONTENT';

export const setFilter = (payload) => ({
  type: SET_FILTER,
  payload,
});

export const setKeyword = (payload) => ({
  type: SET_KEYWORD,
  payload,
});

export const resetFilter = () => ({
  type: RESET_FILTER,
});

export const resetListState = (payload) => ({
  type: RESET_LIST_CONTENT,
  payload,
});

export const setDataListContent = (payload) => ({
  type: SET_DATA_LIST_CONTENT,
  payload,
});

export const getDataListContent = async (saveDataContent) => {
  try {
    const { data } = await axios.get(getListContent);

    await database.write(async () => {
      data.map(async (item) => {
        // get data from local db
        const [isExist] = await contentsCollection
          .query(Q.where('contentId', item.id))
          .fetch();

        // save data to local db
        const saveData = (content) => {
          const _content = content;
          _content.contentId = item.id;
          _content.url = item.url;
          _content.category = item.category;
          _content.category_name = item.category_name;
          _content.isBookmark = false;
        };

        // validate data is exist or not
        if (!isExist) {
          await contentsCollection.create(saveData);
        }
      });
    });
  } catch (error) {
    console.log('getDataListContent error: ', error);
  }
};

export const setDataBookmark = async (data) => {
  await database.write(async () => {
    const content = await contentsCollection.find(data.id);
    await content.update(() => {
      content.isBookmark = !data.isBookmark;
    });

    ToastAndroid.showWithGravity(
      `${data.contentId} ${data.isBookmark ? 'Bookmarked' : 'Unbookmarked'}`,
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
  });
};
