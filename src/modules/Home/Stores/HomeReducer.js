import _ from 'lodash';
import {
  SET_DATA_LIST_CONTENT,
  SET_KEYWORD,
  SET_FILTER,
  RESET_LIST_CONTENT,
  RESET_FILTER,
} from './HomeActions';

export const defaultDataConfig = {
  filter: [],
  keyword: '',
  example: '',
  menuFilter: [
    {
      id: 1,
      name: 'Odd',
      value: 'odd',
      comparator: (x) => x.category_name.toUpperCase() === 'ODD',
    },
    {
      id: 2,
      name: 'Even',
      value: 'even',
      comparator: (x) => x.category_name.toUpperCase() === 'EVEN',
    },
  ],
  listDataContent: [],
};

const HomeReducer = (state = defaultDataConfig, action) => {
  const newState = _.cloneDeep(state);
  switch (action.type) {
    case SET_DATA_LIST_CONTENT:
      newState.listDataContent = action.payload;
      return newState;
    case SET_KEYWORD:
      newState.keyword = action.payload;
      return newState;
    case SET_KEYWORD:
      newState.keyword = action.payload;
      return newState;
    case SET_FILTER:
      newState.filter = action.payload;
      return newState;
    case RESET_LIST_CONTENT:
      return defaultDataConfig;
    case RESET_FILTER:
      newState.filter = [];
      return newState;
    default:
      return state;
  }
};

export default HomeReducer;
