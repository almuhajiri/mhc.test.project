import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { Q } from '@nozbe/watermelondb';
import { View, Text } from 'react-native';
import withObservables from '@nozbe/with-observables';

import Route from '../../../app/Routes';
import HomeComponent from '../Components/HomeComponent';
import {
  setFilter,
  setKeyword,
  setDataBookmark,
  setDataListContent,
  getDataListContent,
  resetFilter,
} from '../Stores/HomeActions';
import { configCollection, contentsCollection } from '../../../app/localDb';

const HomeContainer = (props) => {
  const {
    filter,
    keyword,
    menuFilter,
    getDataList,
    saveDataContent,
    handleSetKeyword,
    handleResetFilter,
    handleDetailContent,
    contents: dataContent,
    handleSetFilter,
  } = props;

  const [refreshing, setRefreshing] = React.useState(false);

  React.useEffect(() => {
    getDataListContent(saveDataContent);
  }, []);

  let _dataContent = [];

  if (filter.length > 0) {
    filter?.forEach((filterValue) => {
      const { comparator } = menuFilter?.find(
        (x) => x.value === filterValue ?? {},
      );
      _dataContent = _dataContent.concat(dataContent?.filter(comparator));
    });
  } else {
    _dataContent = dataContent;
  }

  const listDataContent = _.uniqBy(_dataContent, (x) => x.contentId);

  const onRefresh = () => {
    setRefreshing(true);
    getDataListContent(saveDataContent);
    setRefreshing(false);
  };

  return (
    <HomeComponent
      filter={filter}
      keyword={keyword}
      onRefresh={onRefresh}
      menuFilter={menuFilter}
      refreshing={refreshing}
      setDataBookmark={setDataBookmark}
      handleSetFilter={handleSetFilter}
      listDataContent={listDataContent}
      handleSetKeyword={handleSetKeyword}
      handleResetFilter={handleResetFilter}
      handleDetailContent={handleDetailContent}
    />
  );
};

const stateMapper = (state) => ({
  ...state.HomeReducer,
});

const dispatchMapper = (dispatch, props) => {
  const { navigation } = props;
  return {
    handleResetFilter: () => dispatch(resetFilter()),
    handleSetFilter: (filter) => dispatch(setFilter([filter])),
    saveDataContent: (data) => dispatch(setDataListContent(data)),
    handleDetailContent: (data) =>
      navigation.navigate(Route.DetailContent, { data }),
    handleSetKeyword: (keyword) => dispatch(setKeyword(keyword)),
  };
};

const enhance = withObservables(['keyword'], (props) => {
  const { keyword } = props;

  const wheres = [];

  if (keyword) {
    wheres.push(
      Q.and(Q.where('contentId', Q.like(`%${Q.sanitizeLikeString(keyword)}%`))),
    );
  }

  return {
    contents: contentsCollection
      .query(...wheres)
      .observeWithColumns(['contentId', 'isBookmark']),
  };
});

export default connect(stateMapper, dispatchMapper)(enhance(HomeContainer));
