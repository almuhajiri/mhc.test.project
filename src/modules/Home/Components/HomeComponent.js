import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Themes from '../../../app/Themes';

const HomeComponent = (props) => {
  const {
    filter,
    keyword,
    onRefresh,
    refreshing,
    menuFilter,
    handleSetFilter,
    setDataBookmark,
    listDataContent,
    handleSetKeyword,
    handleResetFilter,
    handleDetailContent,
  } = props;

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => handleDetailContent(item._raw)}
      style={{
        marginBottom: 4,
        alignItems: 'center',
        flexDirection: 'row',
      }}
    >
      <Image source={{ uri: item.url }} style={{ width: 80, height: 80 }} />
      <View style={{ marginLeft: 8 }}>
        <Text style={{ color: Themes.COLORS.black }}>
          {['Name : ', `${item.contentId}`]}
        </Text>
        <Text style={{ color: Themes.COLORS.black }}>
          {['Description : ', `${item.category}`]}
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => setDataBookmark(item)}
        style={{ marginLeft: 10, position: 'absolute', right: 0 }}
      >
        {item.isBookmark ? (
          <Icon name="bookmark" size={30} color={Themes.COLORS.black} />
        ) : (
          <Icon name="bookmark-outline" size={30} color={Themes.COLORS.black} />
        )}
      </TouchableOpacity>
    </TouchableOpacity>
  );

  return (
    <View style={{ flex: 1, backgroundColor: Themes.COLORS.white }}>
      <View
        style={{
          paddingVertical: 10,
          paddingHorizontal: 16,
          justifyContent: 'center',
        }}
      >
        <TextInput
          style={{
            paddingVertical: 4,
            borderRadius: 10,
            paddingHorizontal: 18,
            color: Themes.COLORS.black,
            backgroundColor: Themes.COLORS.grey3,
          }}
          placeholder="Search"
          placeholderTextColor={Themes.COLORS.grey2}
          value={keyword}
          onChangeText={(text) => handleSetKeyword(text)}
        />
        <Icon
          name="magnify"
          size={18}
          color="black"
          style={{ position: 'absolute', right: 26 }}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 16,
          paddingBottom: 14,
        }}
      >
        {menuFilter.map((item) => {
          return (
            <TouchableOpacity
              key={item.id}
              onPress={() =>
                filter.find((x) => x === item.value)
                  ? handleResetFilter()
                  : handleSetFilter(item.value)
              }
              style={{
                flex: 1,
                marginTop: 8,
                borderWidth: 1,
                marginRight: 8,
                borderRadius: 10,
                paddingVertical: 8,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: filter.find((x) => x === item.value)
                  ? Themes.COLORS.blue
                  : 'white',
                borderColor: filter.find((x) => x === item.value)
                  ? Themes.COLORS.blue
                  : Themes.COLORS.grey2,
              }}
            >
              <Text
                style={{
                  color: filter.find((x) => x === item.value)
                    ? Themes.COLORS.white
                    : Themes.COLORS.grey2,
                }}
              >
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        ListEmptyComponent={() => (
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <Text style={{ color: Themes.COLORS.grey2 }}>Data Empty</Text>
          </View>
        )}
        data={listDataContent}
        renderItem={renderItem}
        keyExtractor={(item) => item?.id}
        ItemSeparatorComponent={() => <View style={{ height: 1 }} />}
        contentContainerStyle={{ paddingVertical: 12, paddingHorizontal: 16 }}
      />
    </View>
  );
};

export default HomeComponent;
