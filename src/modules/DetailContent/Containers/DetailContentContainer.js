import React from 'react';
import { connect } from 'react-redux';
import { Q } from '@nozbe/watermelondb';
import { View, Text } from 'react-native';
import withObservables from '@nozbe/with-observables';

import { setDataBookmark } from '../../Home/Stores/HomeActions';
import DetailContentComponent from '../Components/DetailContentComponent';
import { contentsCollection } from '../../../app/localDb';

const DetailContentContainer = (props) => {
  const {
    route: {
      params: { data },
    },
  } = props;
  return (
    <DetailContentComponent {...props} setDataBookmark={setDataBookmark} />
  );
};

const stateMapper = (state) => ({
  ...state.DetailContentReducer,
});

const dispatchMapper = (dispatch) => ({});

const enhance = withObservables([], (props) => {
  const {
    route: {
      params: { data },
    },
  } = props;
  return {
    dataContent: contentsCollection
      .query(Q.where('contentId', data.contentId))
      .observeWithColumns(['contentId', 'isBookmark']),
  };
});

export default connect(
  stateMapper,
  dispatchMapper,
)(enhance(DetailContentContainer));
