import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Themes from '../../../app/Themes';
import Image from '../../../components/Image';

const DetailContentComponent = (props) => {
  const {
    dataContent: [dataContent],
    setDataBookmark,
  } = props;
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      }}
    >
      <View />
      <Image imageUrl={dataContent.url} styles={{ width: 300, height: 300 }} />
      <TouchableOpacity
        style={{
          borderRadius: 6,
          paddingVertical: 12,
          alignItems: 'center',
          paddingHorizontal: 24,
          justifyContent: 'center',
          backgroundColor: Themes.COLORS.white,
        }}
        onPress={() => setDataBookmark(dataContent)}
      >
        <Text style={{ color: Themes.COLORS.black, fontSize: 21 }}>
          {dataContent.isBookmark ? 'Remove Bookmark' : 'Add Bookmark'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default DetailContentComponent;
