import _ from 'lodash';

export const defaultDataConfig = {
  example: '',
  dataContent: {},
};

const DetailContentReducer = (state = defaultDataConfig, action) => {
  const newState = _.cloneDeep(state);
  switch (action.type) {
    default:
      return state;
  }
};

export default DetailContentReducer;
